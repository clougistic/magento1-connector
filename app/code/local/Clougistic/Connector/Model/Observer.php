<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Observer
 *
 */
class Clougistic_Connector_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderPlaceAfter(Varien_Event_Observer $observer)
    {	
        $order = $observer->getEvent()->getOrder();
        Mage::getSingleton('clougistic_connector/api')->syncOrder($order);
	}

    /**
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderSaveAfter(Varien_Event_Observer $observer)
    {
		$order = $observer->getEvent()->getOrder();
		
		if (Mage::helper('core')->isModuleEnabled('Bc_Deliverydate')) {
			$helper = Mage::helper('clougistic_connector/api');
			if ($helper->isAdmin()) {
				Mage::helper('deliverydate')->saveShippingArrivalDateAdmin($observer);
			} else {
				Mage::helper('deliverydate')->saveShippingArrivalDate($observer);
			}
		}
		
        Mage::getSingleton('clougistic_connector/api')->updateOrder($order);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
	public function salesCreditmemoSaveAfter(Varien_Event_Observer $observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        Mage::getSingleton('clougistic_connector/api')->syncCreditmemo($creditmemo);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
	public function controllerActionPostDispatch(Varien_Event_Observer $observer)
	{
		$request = $observer->getEvent()->getControllerAction()->getRequest();
		$action = $observer->getEvent()->getControllerAction()->getFullActionName();
		
		switch ($action) {
		case 'adminhtml_sales_order_addressSave':
			$addressId = $request->getParam('address_id');
			if ($addressId) {
				$address = Mage::getModel('sales/order_address')->load($addressId);
				if ($address) {
					$order = $address->getOrder();
					if ($order) {
						Mage::getSingleton('clougistic_connector/api')->updateOrder($order, true);
					}
				}
			}
		break;
		}
	}

    /**
     * @param Varien_Event_Observer $observer
     */
    public function addClougisticGridColumn(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('clougistic_connector/api');
		if (!$helper->isEnabled()) {
            return;
        }

        $block = $observer->getBlock();

        switch (true) {
        case $block instanceof Mage_Adminhtml_Block_Sales_Order_Grid:
           $block->addColumnAfter('cg_status', array(
                'header'   => 'Clougistic',
                'type'     => 'options',
                'index'    => 'cg_status',
                'filter_index' => 'main_table.cg_status',
                'width'    => '40px',
                'align'    => 'center',
                'filter_condition_callback'
                    => array($this, '_filterCgCondition'),
                        'sortable' => false,
                        'renderer' => 'Clougistic_Connector_Block_Adminhtml_Clougistic_Grid_Renderer_Status',
                        'options'  => $helper->getCgStatuses()
                    ), '');
        break;
        case $block instanceof Mage_Adminhtml_Block_Sales_Order_Create_Search_Grid:
            if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
                $block->addColumnAfter('qty_stock',	array(
                    'header' => Mage::helper('catalog')->__('Qty Stock'),
                    'width'  => '30px',
                    'type'   => 'number',
                    'align'  => 'right',
                    'index'  => 'qty_stock',
                    'filter' => false
                ), 'sku');
            }
        break;
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
	public function beforeCollectionLoad(Varien_Event_Observer $observer)
	{
		$collection = $observer->getCollection();
        if (!isset($collection)) {
            return;
        }

		switch (true) {
        case $collection instanceof Mage_Catalog_Model_Resource_Product_Collection:
            if (Mage::app()->getRequest()->getControllerName() == 'sales_order_create') {
                if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
                    $collection->joinField('qty_stock',
                        'cataloginventory/stock_item',
                        'qty',
                        'product_id=entity_id',
                        '{{table}}.stock_id=1',
                        'left');
                }
            }
        break;
        }
	}

    /**
     * @param Varien_Event_Observer $observer
     */
    public function adminhtmlWidgetContainerHtmlBefore(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('clougistic_connector/api');
        if (!$helper->isEnabled()) {
            return;
        }

        $block = $observer->getBlock();

        switch(true) {
        case $block instanceof Mage_Adminhtml_Block_Sales_Order_View:
            $id = $block->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($id);
            if (! in_array($order->getState(), array($order::STATE_COMPLETE, $order::STATE_CLOSED, $order::STATE_CANCELED))) {
                $message = Mage::helper('clougistic_connector')->__('Are you sure you want to (re)sync this order with Clougistic?');
                $block->addButton('cg_sync_action', array(
                    'label'     => Mage::helper('clougistic_connector')->__('Clougistic'),
                    'onclick'   => "confirmSetLocation('{$message}', '{$block->getUrl('*/clougistic_index/syncOrder', ['order_id' => $id])}')",
                    'class'     => 'go'
                ));
            }
        break;
        }
    }

    /**
     * @param $collection
     * @param $column
     */
    public function _filterCgCondition($collection, $column)
    {
        $value = $column->getFilter()->getValue();
		$collection->addFieldToFilter($column->getFilterIndex(), array($value));
    }
}