<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_Selector
 *
 */
class Clougistic_Connector_Model_Carrier_Selector
{
	const CONFIG_PATH_MODULES = 'clougistic_wms/connector/modules';

	public function getOrderOptions($order)
	{
		$shippingMethod = explode('_', $order->getShippingMethod(), 2)[0];
		$logicModel = $this->getLogicModel($shippingMethod);
				
		$options = null;
		if ($logicModel) {
			$data = $logicModel->getOrderOptionData($order);
			if ($data !== null) {
				$options[$shippingMethod] = $data;
			}
		}

		return $options;
	}

	public function getAllDocuments()
	{
		$modules = Mage::getStoreConfig(self::CONFIG_PATH_MODULES);
		$coreHelper = Mage::helper('core');
			
		$documents = $this->getStandardDocuments();
		
		foreach ($modules as $moduleKey => $module) {
			if ($coreHelper->isModuleEnabled($moduleKey)) {
				$logicModel = Mage::getSingleton($module['model']);
				$documents = array_merge($documents, $logicModel->getDocuments());
			}
		}
		
		return $documents;
	}
	
	protected function getLogicModel($carrierCode)
	{
		if ($carrierCode) {
			$modules = Mage::getStoreConfig(self::CONFIG_PATH_MODULES);
			$coreHelper = Mage::helper('core');
			foreach ($modules as $moduleKey => $module) {
				if ($coreHelper->isModuleEnabled($moduleKey)) {
					$methods = explode(',', $module['methods']);
					if (in_array($carrierCode, $methods)) {
						return Mage::getSingleton($module['model']);
					}
				}
			}
		}
		
		return null;
	}

	public function getStandardDocuments()
	{
		return array(
			array('value' => 'mage_*invoice', 	  'label' => 'Magento Invoice'),
			array('value' => 'mage_*packingslip', 'label' => 'Magento Packingslip')
		);
	}
	
	public function before($shipment, $options)
	{
		if ($logicModel = $this->getLogicModel($options->carrier_code)) {
			$method = __FUNCTION__;
			$logicModel->$method($shipment, $options);
		}
	}

	public function afterShipment($shipment, $options)
	{
		if ($logicModel = $this->getLogicModel($options->carrier_code)) {
			$method = __FUNCTION__;
			$logicModel->$method($shipment, $options);
		}

		Mage::getModel('clougistic_connector/carrier_standard_document_packingSlip')->sendToPrinter($shipment, $options);
	}

	public function afterInvoice($invoice, $options)
	{
		if ($logicModel = $this->getLogicModel($options->carrier_code)) {
			$method = __FUNCTION__;
			$logicModel->$method($shipment, $options);
		}

		Mage::getModel('clougistic_connector/carrier_standard_document_invoice')->sendToPrinter($invoice, $options);
	}
}