<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_PostNL_Document_Label
 *
 */
class Clougistic_Connector_Model_Carrier_PostNL_Document_Label extends TIG_PostNL_Model_Core_Label
{
	public function sendToPrinter($shipment, $options)
	{
		if (!isset($options->printnode['api_key']) || !isset($options->carrier_code)) {
			return;
		}

		$printNodeHelper = Mage::helper('clougistic_connector/printNode_config');
		if (!$printNodeHelper->isEnabled()) {
			return;
		}
		
		$helper = Mage::helper('postnl/carrier');
		if (!$helper->isPostnlShippingMethod($shipment->getOrder()->getShippingMethod())) {
			return;
		}

		$serviceModel = Mage::getModel('postnl_core/service_shipment');
		$includeReturnLabels = $helper->canPrintReturnLabelsWithShippingLabels($shipment->getStoreId());
		$labels = $serviceModel->getLabels($shipment, true, $includeReturnLabels);

		$labelCounter = 1;
		foreach ($labels as $label) {		
			$printerId = @$options->printnode['labels'][$options->carrier_code][$label->getLabelType()];
			if ($printerId) {
				if ($printNodeHelper->canPrintDocument($options->carrier_code, $label->getLabelType(), $shipment->getStore())) {
					$jobTitle = sprintf('Shipment %s %s %s (%s)', $shipment->getIncrementId(), $options->carrier_code, $label->getLabelType(), $labelCounter++);
					$printNode = Mage::getSingleton('clougistic_connector/printNode_connector');
					$printNode->setApiKey($options->printnode['api_key']);
					$printNode->submit(
						$label->getLabel(),  # labels are base64 encoded already by PostNL
						$printerId, 
						$jobTitle 
					);	
				}
			}
		}
	}
}