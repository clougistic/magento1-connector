<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_PostNL_Document_PackingSlip
 *
 */
class Clougistic_Connector_Model_Carrier_PostNL_Document_PackingSlip extends TIG_PostNL_Model_Core_PackingSlip
{
	public function sendToPrinter($shipment, $options)
	{
		if (!isset($options->printnode['api_key']) || !isset($options->carrier_code)) {
			return;
		}

		$printNodeHelper = Mage::helper('clougistic_connector/printNode_config');
		if (!$printNodeHelper->isEnabled()) {
			return;
		}
		
		if (!$printNodeHelper->canPrintDocument($options->carrier_code, '*packingslip', $shipment->getStore())) {
			return;
		}
		
		$helper = Mage::helper('postnl/carrier');
		if (!$helper->isPostnlShippingMethod($shipment->getOrder()->getShippingMethod())) {
			return;
		}

		$serviceModel = Mage::getModel('postnl_core/service_shipment');
		$postnlShipment = $serviceModel->loadShipment($shipment->getId(), true);
				
		$pdf = parent::_getPackingSlipPdf($postnlShipment);
		if ($pdf) {
			$printerId = @$options->printnode['labels'][$options->carrier_code]['*packingslip'];
			if ($printerId) {
				$jobTitle = sprintf('Shipment %s PostNL PackingSlip', $shipment->getIncrementId());
				$printNode = Mage::getSingleton('clougistic_connector/printNode_connector');
				$printNode->setApiKey($options->printnode['api_key']);
				$printNode->submit(
					base64_encode($pdf->render()),
					$printerId, 
					$jobTitle
				);	
			}
		}
	}
}