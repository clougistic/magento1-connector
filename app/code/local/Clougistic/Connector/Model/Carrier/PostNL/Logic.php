<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_PostNL_Logic
 *
 */
class Clougistic_Connector_Model_Carrier_PostNL_Logic implements Clougistic_Connector_Model_Carrier_LogicInterface
{
    const ORDER_FIELD_TYPE = 'type';
    const ORDER_FIELD_PRODUCT_CODE = 'product_code';
    const ORDER_FIELD_DELIVERY_DATE = 'delivery_date';
    const ORDER_FIELD_CONFIRM_DATE = 'confirm_date';
    const ORDER_FIELD_IS_PAKJEGEMAK = 'is_pakje_gemak';
    const ORDER_FIELD_IS_PAKKETAUTOMAAT = 'is_pakketautomaat';

    public function getOrderOptionData($order)
    {
		$orderPostNL = Mage::getModel('postnl_core/order')->load($order->getEntityId(), 'order_id');
		
		if ($orderPostNL->getEntityId()) { 
			return array(
				self::ORDER_FIELD_TYPE => $orderPostNL->getType(),
				self::ORDER_FIELD_PRODUCT_CODE => $orderPostNL->getProductCode(),
				self::ORDER_FIELD_DELIVERY_DATE => $orderPostNL->getDeliveryDate(),
				self::ORDER_FIELD_CONFIRM_DATE => $orderPostNL->getConfirmDate(),
				self::ORDER_FIELD_IS_PAKJEGEMAK => $orderPostNL->getIsPakjeGemak(),
				self::ORDER_FIELD_IS_PAKKETAUTOMAAT => $orderPostNL->getIsPakketautomaat()
			);
		}
		
		return null;
    }

	public function getDocuments()
	{
		return array(
			array('value' => 'postnl_*packingslip', 'label' => 'PostNL Packingslip'),
			array('value' => 'postnl_label', 		'label' => 'PostNL Label'),
			array('value' => 'postnl_label-combi', 	'label' => 'PostNL Label-combi'),
			array('value' => 'postnl_codcard', 		'label' => 'PostNL COD Card'),
			array('value' => 'postnl_return_label', 'label' => 'PostNL Return Label')
		);
	}
	
	public function before($shipment, $options)
	{
		if (!is_numeric(@$options->parcel_count)) {
			Mage::throwException(sprintf('Invalid value for Parcel Count [%s]', @$options->parcel_count));
		}
		
		# register postnl data which normally would come from the shipment creation page.
		# ie. parcel_count, product_option and extra_cover_amount
	
		Mage::register('postnl_product_option', @$options->carrier_options['product_option']);
		
		$additionalOptions = $options;
		unset($additionalOptions->carrier_options['product_option']);
		
		Mage::register('postnl_additional_options', @$additionalOptions->carrier_options);
	}
	
	public function afterShipment($shipment, $options) 
	{
		# print postnl labels and packingslip
		Mage::getModel('clougistic_connector/carrier_postNL_document_label')->sendToPrinter($shipment, $options);
		Mage::getModel('clougistic_connector/carrier_postNL_document_packingSlip')->sendToPrinter($shipment, $options);
	}
}
