<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_LogicInterface
 *
 */
interface Clougistic_Connector_Model_Carrier_LogicInterface
{
	public function before($shipment, $options);
	public function afterShipment($shipment, $options);
	
	public function getOrderOptionData($order);
	public function getDocuments();
}