<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_Standard_Document_PackingSlip 
 *
 */
class Clougistic_Connector_Model_Carrier_Standard_Document_PackingSlip
{
	public function sendToPrinter($shipment, $options)
	{
		if (!isset($options->printnode['api_key'])) {
			return;
		}

		$printNodeHelper = Mage::helper('clougistic_connector/printNode_config');
		if (!$printNodeHelper->isEnabled()) {
			return;
		}
		
		if (!$printNodeHelper->canPrintDocument('mage', '*packingslip', $shipment->getStore())) {
			return;
		}
		
		$pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf(array($shipment));
		if ($pdf) {
			$printerId = @$options->printnode['labels']['mage']['*packingslip'];
			if ($printerId) {
				$jobTitle = sprintf('Shipment %s Standard PackingSlip', $shipment->getIncrementId());
				$printNode = Mage::getSingleton('clougistic_connector/printNode_connector');
				$printNode->setApiKey($options->printnode['api_key']);
				$printNode->submit(
					base64_encode($pdf->render()),
					$printerId, 
					$jobTitle
				);	
			}
		}
	}
}