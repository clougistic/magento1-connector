<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_Standard_Logic
 *
 */
class Clougistic_Connector_Model_Carrier_Standard_Logic implements Clougistic_Connector_Model_Carrier_LogicInterface
{
    public function getOrderOptionData($order)
    {
		return null;
    }

	public function getDocuments()
	{
		return array(
			array('value' => 'mage_*invoice', 	  'label' => 'Magento Invoice'),
			array('value' => 'mage_*packingslip', 'label' => 'Magento Packingslip')
		);
	}
	
	public function before($shipment, $options)
	{
	}
	
	public function afterShipment($shipment, $options) 
	{
	}
}
