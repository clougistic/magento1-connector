<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_Standard_Document_Invoice 
 *
 */
class Clougistic_Connector_Model_Carrier_Standard_Document_Invoice
{
	public function sendToPrinter($invoice, $options)
	{
		if (!isset($options->printnode['api_key'])) {
			return;
		}

		$printNodeHelper = Mage::helper('clougistic_connector/printNode_config');
		if (!$printNodeHelper->isEnabled()) {
			return;
		}
		
		if (!$printNodeHelper->canPrintDocument('mage', '*invoice', $invoice->getStore())) {
			return;
		}
		
		$pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf(array($invoice));
		if ($pdf) {
			$printerId = @$options->printnode['labels']['mage']['*invoice'];
			if ($printerId) {
				$jobTitle = sprintf('Invoice %s Standard', $invoice->getIncrementId());
				$printNode = Mage::getSingleton('clougistic_connector/printNode_connector');
				$printNode->setApiKey($options->printnode['api_key']);
				$printNode->submit(
					base64_encode($pdf->render()),
					$printerId, 
					$jobTitle
				);	
			}
		}
	}
}