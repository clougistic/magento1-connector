<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_Usa_Document_Label
 *
 */
class Clougistic_Connector_Model_Carrier_Usa_Document_Label
{
	public function sendToPrinter($shipment, $options)
	{
		if (!isset($options->printnode['api_key']) || !isset($options->carrier_code)) {
			return;
		}

		$printNodeHelper = Mage::helper('clougistic_connector/printNode_config');
		if (!$printNodeHelper->isEnabled()) {
			return;
		}
		
		$labels = $shipment->getShippingLabel();
		
		if (strlen($labels) > 0) {
			$printerId = @$options->printnode['labels'][$options->carrier_code]['*label'];
			if ($printerId) {
				if ($printNodeHelper->canPrintDocument($options->carrier_code, '*label', $shipment->getStore())) {
					$jobTitle = sprintf('Shipment %s %s Label', $shipment->getIncrementId(), $options->carrier_code);
					$printNode = Mage::getSingleton('clougistic_connector/printNode_connector');
					$printNode->setApiKey($options->printnode['api_key']);
					$printNode->submit(
						base64_encode($labels),
						$printerId, 
						$jobTitle 
					);	
				}
			}
		}
	}
}