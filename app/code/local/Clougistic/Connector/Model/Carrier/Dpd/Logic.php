<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_Dpd_Logic
 *
 */
class Clougistic_Connector_Model_Carrier_Dpd_Logic implements Clougistic_Connector_Model_Carrier_LogicInterface
{
    public function getOrderOptionData($order)
    {
		return null;
    }
	
	public function getDocuments()
	{
		return array(
			array('value' => 'dpdclassic_*label', 'label' => 'DPD Classic Label')
		);
	}
	
	public function before($shipment, $options)
	{
		if (!is_numeric(@$options->parcel_count)) {
			Mage::throwException(sprintf('Invalid value for Parcel Count [%s]', @$options->parcel_count));
		}
		
		# register dpd data which normally would come from the shipment creation page.
		# ie. parcel_count
		
		$shipment->setDpdPackageCount(@$options->parcel_count);
	}
	
	public function afterShipment($shipment, $options) 
	{
		# print dpd label
		Mage::getModel('clougistic_connector/carrier_dpd_document_label')->sendToPrinter($shipment, $options);
	}
}
