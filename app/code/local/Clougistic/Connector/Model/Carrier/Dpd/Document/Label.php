<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Carrier_Dpd_Document_Label
 *
 */
class Clougistic_Connector_Model_Carrier_Dpd_Document_Label
{
	public function sendToPrinter($shipment, $options)
	{
		if (!isset($options->printnode['api_key']) || !isset($options->carrier_code)) {
			return;
		}

		$printNodeHelper = Mage::helper('clougistic_connector/printNode_config');
		if (!$printNodeHelper->isEnabled()) {
			return;
		}
		
		$result = Mage::getModel('dpd/adminhtml_dpdgrid')->generateLabelAndCompleteOrder($shipment->getOrder(), $shipment);
		if ($result) {
			if (strlen($shipment->getDpdShippingLabel()) > 0) {
				$printerId = @$options->printnode['labels'][$options->carrier_code]['*label'];
				if ($printerId) {
					if ($printNodeHelper->canPrintDocument($options->carrier_code, '*label', $shipment->getStore())) {
						$jobTitle = sprintf('Shipment %s %s Label', $shipment->getIncrementId(), $options->carrier_code);
						$printNode = Mage::getSingleton('clougistic_connector/printNode_connector');
						$printNode->setApiKey($options->printnode['api_key']);
						$printNode->submit(
							base64_encode($shipment->getDpdShippingLabel()),
							$printerId, 
							$jobTitle 
						);	
					}
				}
			}
		}
		else {
		    $output = '';
			foreach (Mage::getSingleton('core/session')->getMessages()->getItems() as $message) {
				$output .= $message->getText() . "<br>\r\n";
			}
			Mage::throwException($output);
		}
	}
}