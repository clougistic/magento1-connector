<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_Api
 *
 */
class Clougistic_Connector_Model_Api extends Mage_Api_Model_Resource_Abstract
{
    const ERROR_TITLE = 'Clougistic Error';
    const ERROR_LOGFILE = 'clougistic_connector.log';

    const USER_AGENT = 'Clougistic/Connector';

    const URL_API_VERSION = '/api/v1';

    const GREETINGS_METHOD = '/greetings';
    const GREETINGS_METHOD_EXPECTED_RESPONSE = "You're welcome!";

    const TEST_CLIENT_CALLBACK_METHOD = '/test-client-callback';
    const TEST_CLIENT_CALLBACK_MESSAGE = 'Passed: I called myself!';

    const ORDER_METHOD_SYNC = '/order/sync';
    const ORDER_METHOD_CREATE_RMA = '/order/create-rma';
    const TASKS_METHOD_EXECUTE_SOURCE_HOST = '/system/task/execute-source-host';
    const TASKS_METHOD_RETRY_SOURCE_HOST = '/system/task/retry-source-host';

    const STATUS_SUCCESS = 'success';                       // successful response code
    const STATUS_SUCCESS_OFFLOADED = 'success-offloaded';   // successful response code for offloaded processing
    const STATUS_FAIL = 'fail';                             // invalid data or call conditions
    const STATUS_ERROR = 'error';                           // server error
    const STATUS_WARNING = 'warning';                       // warning
    const STATUS_TEXT_OK = 'OK';

	const PRODUCT_METHOD_SYNC = '/product/sync';
	const PRODUCT_METHOD_SYNC_STATUS_CREATED = 'created';
	const PRODUCT_METHOD_SYNC_STATUS_SKIPPED = 'skipped';
	const PRODUCT_METHOD_SYNC_STATUS_ERROR = 'error';

	const PRODUCT_REPLENISHMENT_POLICY_NONE = 0;
	const PRODUCT_REPLENISHMENT_POLICY_MINMAX = 1;
	
	const CLOUGISTIC_CONNECTOR_FLAG_SYNC_ORDER = 'clougistic_connector_flag_sync_order';
	const CLOUGISTIC_CONNECTOR_FLAG_SYNC_CREDITMEMO = 'clougistic_connector_flag_sync_creditmemo';

	const RESPONSE_STATUS_SUCCESS = 'success';
	const RESPONSE_STATUS_ERROR_GRACEFULL = 'error-graceful';
	const RESPONSE_STATUS_ERROR = 'error';

    protected $_helper;
    protected $_source;
    protected $_errors = null;

    public function __construct()
    {
        $this->_helper = Mage::helper('clougistic_connector/api');
    }

    ##################################################
    ##########[ CLOUGISTIC API METHODS ]##############
    ##################################################

    /**
     * Test Clougistic connectivity
     * @param $info
     * @return bool
     */
    public function greetings(& $systemInfo)
    {
		$helper = $this->_helper;
        $systemInfo = null;

		if (!$helper->isEnabled()) {
			return false;
		}
			
        $result = $this->call(self::GREETINGS_METHOD);
        if (@$result->status == self::STATUS_SUCCESS) {
            $systemInfo = (object)$result->data->system_info;
            return $result->data->answer == self::GREETINGS_METHOD_EXPECTED_RESPONSE;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function testCallback()
    {
        $result = $this->call(self::TEST_CLIENT_CALLBACK_METHOD);
        if (isset($result->status)) {
            return __FUNCTION__ . ': ' . $result->data;
        }

        return __FUNCTION__ . ': invalid response. check your connector settings.';
    }

    /**
     * Sync Order with Clougistic
     *
     * @param Mage_Sales_Model_Order $order
     * @param bool $force
     */
    public function syncOrder (Mage_Sales_Model_Order $order)
    {
		$helper = $this->_helper;
		$store = $order->getStore();
		
		# catch recursion
		if (Mage::registry(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_ORDER)) {
            return;
        }
		
		# is connector enabled?
		if (!$helper->isEnabled()) {
            return;
        }

		# do not sync excluded shipping methods
		if ($helper->isExcludedShippingMethod($order)) {
			return;
		}
		
		$paymentMethod = strtolower($order->getPayment()->getMethodInstance()->getCode());

		# MultiSafePay (msp) check if order has been paid
		if (stripos($paymentMethod, 'msp_') !== false && $paymentMethod != 'msp_banktransfer') {
			if ($order->getBaseTotalDue() > 0) {
				return;
			}
		}		
		
		# Klarna check if order has been paid
		if (stripos($paymentMethod, 'klarna_') !== false) {
			if ($order->getBaseTotalDue() > 0) {
				return;
			}
		}
		
		Mage::register(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_ORDER, true);

		$data = $this->_helper->prepareSyncOrder($order);
		
		if ($this->_helper->isValidOrderData($data)) {
			$result = $this->call(self::ORDER_METHOD_SYNC, $data, $store);
			
			switch (@$result->status) {
			case self::STATUS_SUCCESS_OFFLOADED:
                $message = 'Pending Order sync with Clougistic.';
                $order->setCgStatus($helper::CLOUGISTIC_SYNC_PENDING);
                $order->addStatusHistoryComment($message);
                $order->save();
                $this->displaySuccess($message);
                break;
            case self::STATUS_SUCCESS:
                $message = 'Order has been synced with Clougistic.';
                $order->setCgStatus($helper::CLOUGISTIC_SYNCED);
                $order->addStatusHistoryComment($message);
                $order->save();
                $this->displaySuccess($message);
                break;
            case self::STATUS_FAIL:
                $order->setCgStatus($helper::CLOUGISTIC_ERROR);
                $order->addStatusHistoryComment('Order can not be synced with Clougistic. ' . $result->data);
                $order->save();
            default:
				$this->displayErrors();
				break;
			}
		}
		else {
            $message = 'Order contains invalid or missing data and can not be synced with Clougistic.';
			$order->setCgStatus($helper::CLOUGISTIC_INVALID);
			$order->addStatusHistoryComment($message);
			$order->save();

			$this->setErrors($message);
			$this->displayErrors();
		}
	
		Mage::unregister(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_ORDER);
    }
	
	/**
     * Update Order in Clougistic
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function updateOrder (Mage_Sales_Model_Order $order, $manual = false)
    {
        $helper = $this->_helper;
		$store = $order->getStore();

        # only if there is a state change, resync clougistic order
        if (!$manual) {
			if ($order->getOrigData('state') == $order->getState()) {
				return;
			}
		}

        # don't resync POS orders
		$posEnabled = $helper->isPOSEnabled($store);
		if ($posEnabled && $helper->isPOSOrder($order)) {
			return;
		}

		$this->syncOrder($order);
    }

	/**
     * Sync RMA with Clougistic
     *
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
	 * @param boolean $allItems
     */
	public function syncCreditmemo (Mage_Sales_Model_Order_Creditmemo $creditmemo, $allItems = false)
	{
		$helper = $this->_helper;
		$store = $creditmemo->getStore();

		# catch recursion
		if (Mage::registry(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_CREDITMEMO)) {
            return;
        }
		
		if (!$helper->isEnabled()) {
            return;
        } 

		Mage::register(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_CREDITMEMO, true);
		
        $data = $this->_helper->prepareSyncCreditmemo($creditmemo, $allItems);

		if ($this->_helper->isValidOrderRmaData($data))
		{
		    # Creditmemo in Magento is converted to an cancellation/rma in Clougistic
			$result = $this->call(self::ORDER_METHOD_CREATE_RMA, $data, $store);

			switch (@$result->status) {
            case self::STATUS_SUCCESS_OFFLOADED:
                $message = 'Pending creditmemo sync with Clougistic.';
                $creditmemo->addComment($message, false, false);
                $creditmemo->save();
                $this->displaySuccess($message);
                break;
            case self::STATUS_SUCCESS:
                $message = 'Creditmemo has been successfully synced with Clougistic.';
                $creditmemo->addComment($message, false, false);
                $creditmemo->save();
                $this->displaySuccess($message);
                break;
            default:
				$creditmemo->addComment('Creditmemo can not be synced with Clougistic.', false, false);
				$creditmemo->save();
				$this->displayErrors();
			    break;
			}
		}

		Mage::unregister(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_CREDITMEMO);
	}

    /**
     * Execute pending tasks in clougistic and sends the result to us at executeTasksResult()
     */
    public function executeTasks()
    {
        $helper = $this->_helper;
        $store = Mage::app()->getStore();

        if (!$helper->isEnabled()) {
            return;
        }

        if (!$helper->isTasksEnabled()) {
            return;
        }

        $data = array(
            'batch_size' => $helper->getTasksBatchSize()
        );

        $result = $this->call(self::TASKS_METHOD_EXECUTE_SOURCE_HOST, $data, $store);

        switch ($result->status) {
            case self::STATUS_SUCCESS:
            case self::STATUS_SUCCESS_OFFLOADED:
                break;
            default:
                $this->_debugInfo(__FUNCTION__ . sprintf(': error while triggering execution of tasks. [RX=%s]', $result->data));
                break;
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function retryTasks()
    {
        $helper = $this->_helper;
        $store = Mage::app()->getStore();

        if (!$helper->isEnabled()) {
            return __FUNCTION__ . ': Clougistic connector module is not enabled.';
        }

        if (!$helper->isTasksEnabled()) {
            return __FUNCTION__ . ': Clougistic tasks are not enabled.';
        }

        $result = $this->call(self::TASKS_METHOD_RETRY_SOURCE_HOST, null, $store);

        return __FUNCTION__ . ': ' . $result->data;
    }

    ##################################################
    ##############[ XMP-RPC METHODS ]#################
    ##################################################

    /**
     * Receive task data started with executeTasks() method above.
     * Each task has a private method and parameters which is executed in this class scope using $this->taskMethod(Params)
     *
     * @param array $data
     * @return array
     */
    public function executeTasksResult(array $data = array())
    {
        try {
            $data = (object)$data;
            $helper = $this->_helper;

            if (!$helper->isEnabled()) {
                Mage::throwException(__FUNCTION__ . ': Clougistic connector module is not enabled.');
            }

            if (!isset($data->tasks)) {
                Mage::throwException(__FUNCTION__ . ': missing task data.');
            }

            $result = [];
            foreach ($data->tasks as $index => $task)
            {
                # try/catch scoped to single task
                try {
                    $message = null;
                    $success = call_user_func([$this, $this->getTaskMethodName($task['method'])], (array)$task['params']);
                }
                catch (Exception $e) {
                    $success = false;
                    $message = $e->getMessage();
                }
                $result[] = array(
                    'id' => $task['id'],
                    'message' => $message,
                    'success' => $success
                );
            }

            $result['status'] = self::RESPONSE_STATUS_SUCCESS;
            $result['data'] = $result;
        }
        catch (Exception $e) {
            $result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $method
     * @return string
     */
    private function getTaskMethodName($method)
    {
        return sprintf('task%s', ucfirst($method));
    }

    /**
     * TASK Update Magento product.
     * Method callable by executeTasksResult()
     *
     * @param array $data
     * @return bool
     * @throws Mage_Api_Exception
     */
    private function taskUpdateProduct(array $data = array())
    {
        $data = (object)$data;

        if (!isset($data->sku)) {
            Mage::throwException('missing sku for stock update.');
        }

        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data->sku);
        if (!$product) {
            Mage::throwException(sprintf('sku [%s] not found.', $data->sku));
        }

        # eta field
        if (isset($data->eta_field) && isset($data->eta)) {
            $product->setData($data->eta_field, $data->eta);
            $product->getResource()->saveAttribute($product, $data->eta_field);
        }

        # cost field
        if (isset($data->cost)) {
            $product->setCost((float)$data->cost);
            $product->getResource()->saveAttribute($product, 'cost');
        }

        # stock qty
        if (isset($data->qty)) {
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if ($stockItem) {
                $stockItem->setQty((float)$data->qty);
                if ($data->qty > 0) {
                    $stockItem->setIsInStock(1);

                    # set eta to null if there is stock
                    if (isset($data->eta_field)) {
                        $product->setData($data->eta_field, null);
                        $product->getResource()->saveAttribute($product, $data->eta_field);
                    }
                }
                //$stockItem->setProcessIndexEvents(true);
                $stockItem->save();

                // Lesti FPC - Invalidate cache for this product
                if (Mage::helper('core')->isModuleEnabled('Lesti_Fpc')) {
                    $fpc = Mage::getSingleton('fpc/fpc');
                    if ($fpc) {
                        $fpc->clean(sha1('product_' . $product->getId()));
                    }
                }
            }
        }

        return true;
    }

    /**
     * TASK Update Order Item Cost.
     * Method callable by executeTasksResult()
     *
     * @param array $data
     * @return mixed
     * @throws Mage_Api_Exception
     */
    private function taskUpdateOrderItemCost(array $data = array())
    {
        try
        {
            $data = (object)$data;
            $helper = $this->_helper;

            if (!isset($data->reference)) {
                Mage::throwException(__FUNCTION__ . ': missing order item reference.');
            }

            if (!isset($data->cost)) {
                Mage::throwException(__FUNCTION__ . ': missing cost.');
            }

            $orderItem = Mage::getModel('sales/order_item')->load($data->reference);

            if (!$orderItem) {
                Mage::throwException(__FUNCTION__ . sprintf(': order item with reference [%s] does not exist.', $data->reference));
            }

            $orderItem->setCgCost((float)$data->cost);
            $orderItem->getResource()->saveAttribute($orderItem, 'cg_cost');

            $result['status'] = self::RESPONSE_STATUS_SUCCESS;
        }
        catch (Exception $e) {
            $result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Retrigger task execution immediately
     *
     * @return array
     */
    public function executeTasksRetrigger()
    {
        try {
            $helper = $this->_helper;

            if (!$helper->isEnabled()) {
                Mage::throwException(__FUNCTION__ . ': Clougistic connector module is not enabled.');
            }

            $this->executeTasks();

            $result['status'] = self::RESPONSE_STATUS_SUCCESS;
            $result['data'] = $result;
        }
        catch (Exception $e) {
            $result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Test callback from Clougistic.
     *
     * @return mixed
     */
    public function testClientCallback()
    {
        try {
            $helper = $this->_helper;

            if (!$helper->isEnabled()) {
                Mage::throwException(__FUNCTION__ . ': Clougistic connector module is not enabled.');
            }

            $result['status'] = self::RESPONSE_STATUS_SUCCESS;
            $result['data'] = self::TEST_CLIENT_CALLBACK_MESSAGE;

        } catch (Exception $e) {
            $result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Notification after order sync from Clougistic
     *
     * @param array $data
     * @return mixed
     */
    public function orderNotification(array $data = array())
    {
        try {
            $data = (object)$data;
            $helper = $this->_helper;

            if (!$helper->isEnabled()) {
                Mage::throwException(__FUNCTION__ . ': Clougistic connector module is not enabled.');
            }

            if (!isset($data->reference)) {
                Mage::throwException(__FUNCTION__ . ': missing order reference.');
            }

            if (!isset($data->validation_hash)) {
                Mage::throwException(__FUNCTION__ . ': missing validation hash.');
            }

            if (!isset($data->status)) {
                Mage::throwException(__FUNCTION__ . ': missing status.');
            }

            $order = Mage::getModel('sales/order')->loadByIncrementId($data->reference);

            if (!$order->getId()) {
                Mage::throwException(__FUNCTION__ . sprintf(': order reference [%s] not found.', $data->reference));
            }

            if ($order->getCgStatus() != $helper::CLOUGISTIC_SYNC_PENDING) {
                Mage::throwException(__FUNCTION__ . ': current Clougistic order status can not be changed.');
            }

            if ($data->status == self::STATUS_SUCCESS) {
                # if received hash is different from current hash, something changed in the Magento order
                # and we need to resync
                $currentHash = $helper->getOrderValidationHash($order);
                if ($currentHash != $data->validation_hash) {
                    $this->syncOrder($order);
                } else {
                    $order->setCgStatus($helper::CLOUGISTIC_SYNCED);
                    $order->addStatusHistoryComment(sprintf('Order has been synced with Clougistic. [RX=%s]', $data->status_text));
                    $order->save();
                }
            }
            else {
                $order->setCgStatus($helper::CLOUGISTIC_ERROR);
                $order->addStatusHistoryComment(print_r($data->status_text, true));
                $order->save();
            }

            $result['status'] = self::RESPONSE_STATUS_SUCCESS;
        }
        catch (Exception $e) {
            $result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Notification after rma sync from Clougistic
     *
     * @param array $data
     * @return mixed
     */
    public function orderRmaNotification(array $data = array())
    {
        try {
            $data = (object)$data;
            $helper = $this->_helper;

            if (!isset($data->reference)) {
                Mage::throwException(__FUNCTION__ . ': missing creditmemo reference.');
            }

            if (!$helper->isEnabled()) {
                Mage::throwException(__FUNCTION__ . ': Clougistic connector module is not enabled.');
            }

            $creditmemo = Mage::getModel('sales/order_creditmemo')->load($data->reference, 'increment_id');
            if (!$creditmemo->getId()) {
                Mage::throwException(__FUNCTION__ . sprintf(': creditmemo reference [%s] not found.', $data->reference));
            }

            if ($data->status == self::STATUS_SUCCESS) {
                $message = nl2br(sprintf("Creditmemo successfully synced with Clougistic.\n%s", $data->status_text));
            }
            else {
                $message = sprintf('Creditmemo could not be synced with Clougistic. [RX=%s]', $data->status_text);
            }

            $creditmemo->addComment($message, false, false);
            $creditmemo->save();

            $result['status'] = self::RESPONSE_STATUS_SUCCESS;

        } catch (Exception $e) {
            $result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Create shipment from Clougistic
     *
     * @param array $data [order_reference, shipment_id, carrier_code, carrier_options, parcel_count, print_node_labels[], items [id, qty ...]]
     * @return mixed
     * @throws Mage_Api_Exception
     */
    public function createShipment(array $data = array())
    {
		try
        {
			$data = (object)$data;
			$helper = $this->_helper;
			
			if (!isset($data->shipment_id)) {
				Mage::throwException(__FUNCTION__ . ': missing shipment ID.');
			}
			
			if (!isset($data->order_reference)) {
				Mage::throwException(__FUNCTION__ . ': missing order reference.');
			}
			
			if (!isset($data->items) || count(@$data->items) == 0) {
				Mage::throwException(__FUNCTION__ . ': missing items to ship.');
			}

            if (!$helper->isEnabled()) {
                Mage::throwException(__FUNCTION__ . ': Clougistic connector module is not enabled.');
            }

            $order = Mage::getModel('sales/order')->loadByIncrementId($data->order_reference);

			if (!$order->getId()) {
				Mage::throwException(__FUNCTION__ . sprintf(': order reference [%s] not found.', $data->order_reference));
			}

			$carrierSelector = $helper->getCarrierSelector();
    		$shipmentCreated = false;
			
			if ($order->canShip())
			{

				$order->setIsInProcess(true);
                $order->setCustomerNoteNotify(false);
				
				$itemQty = array();
				foreach ($data->items as $item) {
					$foundItem = false;
					foreach ($order->getAllItems() as $orderItem) {
						if ($orderItem->getItemId() == $item['id']) {
							$shipableQty = $orderItem->getQtyToShip();
							if ($shipableQty < $item['qty']) {
								Mage::throwException(__FUNCTION__ . sprintf(': item [ref=%s, sku=%s] qty [%s] exceeds shippable qty [%s].', $orderItem->getItemId(), $orderItem->getSku(), $item['qty'], $shipableQty));
							}
							$itemQty[$orderItem->getItemId()] = $item['qty'];
							$foundItem = true;
							break;
						}					
					}
					
					if (!$foundItem) {
						Mage::throwException(__FUNCTION__ . sprintf(': item [ref=%s] not found.', $item['id']));
					}
				}
				
				$shipment = $order->prepareShipment($itemQty);
				$shipment->register();
				$shipment->increment_id = sprintf('%s%s', $helper->getShipmentIncrementIdPrefix($order->getStore()), $data->shipment_id);

				$carrierSelector->before($shipment, $data);

				Mage::register(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_ORDER, true);

				Mage::getModel('core/resource_transaction')
					->addObject($shipment)
					->addObject($order)
					->save();

				if (!$shipment->getData('email_sent')) {
					$shipment->sendEmail(true);
					$shipment->setEmailSent(true);
					$shipment->save();
        		}

				$shipmentCreated = true;
			}

			if (!$shipmentCreated) {
				Mage::throwException(__FUNCTION__ . ': the shipment can not be created.');
			}
			
			$result['status'] = self::RESPONSE_STATUS_SUCCESS;
        }
		catch (Exception $e) {
            $result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
        }

		Mage::unregister(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_ORDER);
		
        return $result;
    }
	
	/**
     * Create and print Invoice.
     *
     * @param array $data
     * @return mixed
     * @throws Mage_Api_Exception
     */
	public function createAndPrintInvoice(array $data = array())
	{
		try 
		{
			$data = (object)$data;
			$helper = $this->_helper;

            if (!isset($data->order_reference)) {
                Mage::throwException(__FUNCTION__ . ': missing order reference.');
            }

            if (count($data->items) == 0) {
				Mage::throwException(__FUNCTION__ . ': missing items to invoice.');
			}

            if (!$helper->isEnabled()) {
                Mage::throwException(__FUNCTION__ . ': Clougistic connector module is not enabled.');
            }

            $order = Mage::getModel('sales/order')->loadByIncrementId($data->order_reference);

			if (!$order->getId()) {
				Mage::throwException(__FUNCTION__ . sprintf(': order reference [%s] not found.', $data->order_reference));
			}

			$carrierSelector = $helper->getCarrierSelector();
		
			$itemQty = array();
			foreach ($data->items as $item) {
				foreach ($order->getAllItems() as $orderItem) {
					if ($orderItem->getItemId() == $item['id']) {
						$invoicableQty = $orderItem->getQtyShipped() - $orderItem->getQtyInvoiced();
						if ($invoicableQty > 0) {
							$itemQty[$orderItem->getItemId()] = $invoicableQty;
						}
						break;
					}					
				}
			}
			
			# are there any items that need be invoiced?
			if (count($itemQty) > 0) 
			{
				$invoice = $order->prepareInvoice($itemQty);
				$order->setIsInProcess(true);

				if ($invoice->canCapture()) {
					if ($order->getPayment()->getMethodInstance()->isGateway()) {
						$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
					} else {
						$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
					}
				} else {
					$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::NOT_CAPTURE);
				}

				$invoice->register();

				Mage::register(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_ORDER, true);
				
				Mage::getModel('core/resource_transaction')
					->addObject($invoice)
					->addObject($order)
					->save();

				if (!$invoice->getData('email_sent')) {
					$invoice->sendEmail(true);
				}

				$carrierSelector->afterInvoice($invoice, $data);
			}

			$result['status'] = self::RESPONSE_STATUS_SUCCESS;
		}
		catch (Exception $e) {
			
            $result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
        }

		Mage::unregister(self::CLOUGISTIC_CONNECTOR_FLAG_SYNC_ORDER);
		
		return $result;		
	}
	
	/**
     * Print shipment labels.
     *
     * @param array $data
     * @return mixed
     * @throws Mage_Api_Exception
     */
	public function printShipmentLabels(array $data = array())
	{
		try 
		{
			$data = (object)$data;
			$helper = $this->_helper;

			if (!isset($data->shipment_id)) {
				Mage::throwException(__FUNCTION__ . ': missing shipment ID.');
			}

            if (!isset($data->order_reference)) {
                Mage::throwException(__FUNCTION__ . ': missing order reference.');
            }

            if (!$helper->isEnabled()) {
                Mage::throwException(__FUNCTION__ . ': Clougistic connector module is not enabled.');
            }

            $order = Mage::getModel('sales/order')->loadByIncrementId($data->order_reference);

			if (!$order->getId()) {
				Mage::throwException(__FUNCTION__ . sprintf(': order reference [%s] not found.', $data->order_reference));
			}
			
			$carrierSelector = $helper->getCarrierSelector();
			
			$incrementId = sprintf('%s%s', $helper->getShipmentIncrementIdPrefix($order->getStore()), $data->shipment_id);
			$shipment = Mage::getModel('sales/order_shipment')->loadByIncrementId($incrementId);
			
			if (!$shipment->getId()) {
				Mage::throwException(__FUNCTION__ . sprintf(': shipment [%s] not found.', $incrementId));
			}

			$carrierSelector->afterShipment($shipment, $data);

			$result['status'] = self::RESPONSE_STATUS_SUCCESS;
		}
		catch (Exception $e) {
			$result['status'] = self::RESPONSE_STATUS_ERROR;
            $result['data'] = $e->getMessage();
		}
		
		return $result;
	}

    ##################################################
    #################[ MISC METHODS ]#################
    ##################################################

    /**
     * @param $method
     * @return string
     */
    public function getApiUrl($method)
    {
        $endPoint = $this->_helper->getUrlEndPoint();
        return rtrim($endPoint, DS) . self::URL_API_VERSION . $method;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return (array)$this->_errors;
    }

    /**
     * @param $errors
     */
    protected function setErrors($errors)
    {
        $this->_errors = $errors;
    }

    /**
     * Add error messages to session for displaying
     */
    public function displayErrors()
    {
		if (!$this->_helper->isAdmin()) {
			return;
		}
		
        $errors = $this->getErrors();

        if ($errors == null) {
            return;
        }

        if (!is_array($errors)) {
            $errors = array($errors);
        }

        $session = Mage::getSingleton('adminhtml/session');
        foreach ($errors as $error) {
            if (is_array($error)) {
                $error = implode(', ', $error);
            }
            $session->addError('[Clougistic Error] ' . $error);
        }
    }

    /**
     * @param $message
     */
    public function displaySuccess($message)
    {
		if (!$this->_helper->isAdmin()) {
			return;
		}

        $session = Mage::getSingleton('adminhtml/session');
        $session->addSuccess('[Clougistic] ' . $message);
    }

    /**
     * Do api call to Clougistic.
     *
     * @param $method string
     * @param $data null
     * @param $store null
     * @return mixed
     */
    protected function call ($method, $data = null, $store = null)
    {
        $this->_errors = null;

        $request['userAgent'] = self::USER_AGENT;
        $request['timestamp'] = time();
        $request['data'] = $data;

        $request = json_encode($request);

        $apiUrl = $this->getApiUrl($method);
	
        $ch = curl_init($apiUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($request),
            'X-Authorization: ' . $this->_helper->getApiKey(),
			'X-Entity-ID: ' . $this->_helper->getEntityId($store),
            'X-Source-Host: ' . $this->_helper->getAdminHostname()
        ));
        $result = curl_exec($ch);
        // debug
        //echo $result;
        if (curl_errno($ch)) {
            $this->setErrors(sprintf('curl: %s [%s]' . curl_error($ch), $apiUrl));
            return null;
        }
        $http_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $resultObj = json_decode($result);
        if (is_object($resultObj)) {
            if (isset($resultObj->status)) {
                $this->_debugInfo($method . ' ' . $resultObj->status);
                $this->_debugInfo($resultObj->data);
                if ($resultObj->status != 'success') {
					if (is_object($resultObj->data)) {
						foreach ($resultObj->data as $key => $value) {
							$this->setErrors(implode(', ', $value));
						}
					}
					else {
						$this->setErrors($resultObj->data . ($this->_helper->isDebug() ? "<br><br>" . implode('<br>', $resultObj->trace) . ")" : ""));
					}
                }

                return $resultObj;
            }
        }
		
        $this->setErrors(sprintf('CURL Unknown Response [status=%s]: %s', $http_status_code, $result));
    }

    /**
     * @param $text
     */
    protected function _debugInfo($text)
    {
		if (!is_string($text)) {
			$text = print_r($text, true);
		}
		$text = date('[Y-m-d H:i:s]') . "\r\n" . $text;
		Mage::log($text, null, self::ERROR_LOGFILE);
    }
}

