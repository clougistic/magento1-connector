<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_PrintNode_Connector
 *
 */
class Clougistic_Connector_Model_PrintNode_Connector extends Mage_Core_Model_Abstract
{
    /**
     * Interface url. http or https calculate by store protocol
     * @var string
     */
    private $_apiUrl = 'https://api.printnode.com';

	private $_apiKey = null;
	
    private $_apiClient = null;

    /**
     * client setter
     *
     * @param null|\Zend_Gdata_HttpClient $gcpClient Client
     */
    public function setApiClient($gcpClient)
    {
        $this->_apiClient = $gcpClient;
    }

	/**
	 * Set Api Key
	 */
	public function setApiKey($apiKey)
	{
		$this->_apiKey = $apiKey;
	}
	
    /**
     * Get client object
     *
     * @return Zend_Gdata_HttpClient
     */
    public function getClient()
    {
        if (is_null($this->_apiClient)) {
			if (!$this->_apiKey) {
				return;
			}
			$client = new Zend_Http_Client($this->_apiUrl, array('adapter' => 'Zend_Http_Client_Adapter_Curl'));
        	$client->setAuth($this->_apiKey, '');
            $this->setApiClient($client);
        }
        return $this->_apiClient;
    }

    /**
     * Submit document to printer
     *
     * @param string $document Document
     * @param string $type Type
     * @param string $title Title
     * @return bool
     */
    public function submit($document, $printerId, $title = 'Print Job', $type = 'pdf_base64')
    {
        if (!$document) {
			return false;
		}
        if (!$printerId) {
			return false;
		}
		$client = $this->getClient();
        if (!$client) {
			return false;
		}
		$client->setUri($this->_apiUrl . '/printjobs');
		$client->setParameterPost('printer', $printerId);
        $client->setParameterPost('title', $title);
        $client->setParameterPost('content', $document);
        $client->setParameterPost('contentType', $type);
        $client->setParameterPost('source', "Clougistic Connector Magento");
        $response = $client->request(Zend_Http_Client::POST);
        return $response->isSuccessful();
    }

    /**
     * Return Printers list
     *
     * @return array
     */
    public function search()
		{
        $client = $this->getClient();
		if (!$client) {
			return false;
		}
        $client->setUri($this->_apiUrl . '/printers');
        $response = $client->request(Zend_Http_Client::GET);
        $printerResponse = json_decode($response->getBody());
        if ($printerResponse) {
            return $printerResponse;
        }
        return false;
    }
}