<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_System_Config_Source_Payment_Method
 *
 */
class Clougistic_Connector_Model_System_Config_Source_Payment_Method
{
    public function toOptionArray($dummy = null, $sort = SORT_ASC)
    {
        $paymentMethods = Mage::getSingleton('payment/config')->getActiveMethods();

        foreach ($paymentMethods as $paymentCode => $methodTitle) {
            $paymentTitle = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
            if (! $paymentTitle) $paymentTitle = $paymentCode;
            $options[] = array(
                'value' => $paymentCode,
                'label' => $paymentTitle
            );
        }

        if ($sort !== false && count($options) > 0) {
            foreach ($options as $key => $row) {
                $label[$key] = $row['label'];
            }

            array_multisort ($label, $sort, $options);
        }

        return $options;
    }
}