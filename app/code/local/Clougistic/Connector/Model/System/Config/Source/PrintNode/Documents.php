<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_System_Config_Source_PrintNode_Documents
 *
 */
class Clougistic_Connector_Model_System_Config_Source_PrintNode_Documents
{
    public function toOptionArray($dummy = null, $sort = SORT_ASC)
    {
		$documents = Mage::helper('clougistic_connector/api')->getCarrierSelector()->getAllDocuments();

		foreach ($documents as $key => $row) {
			$label[$key] = $row['label'];
		}

		array_multisort ($label, $sort, $documents);
		
        return $documents;
    }
}