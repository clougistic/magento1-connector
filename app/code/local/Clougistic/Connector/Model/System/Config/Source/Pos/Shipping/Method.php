<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_System_Config_Source_Pos_Shipping_Method
 *
 */
class Clougistic_Connector_Model_System_Config_Source_Pos_Shipping_Method
{
    public function toOptionArray($dummy = null, $sort = SORT_ASC)
    {
        $shippingMethods = Mage::getSingleton('shipping/config')->getAllCarriers();
		
        foreach ($shippingMethods as $shippingCode => $methodTitle) {
            $shippingTitle = Mage::getStoreConfig('shipping/' . $shippingCode . '/title');
            if (! $shippingTitle) $shippingTitle = $shippingCode;
            $options[] = array(
                'value' => $shippingCode,
                'label' => $shippingTitle
            );
        }

        if ($sort !== false && count($options) > 0) {
            foreach ($options as $key => $row) {
                $label[$key] = $row['label'];
            }

            array_multisort ($label, $sort, $options);
        }

        return $options; 
    }
}