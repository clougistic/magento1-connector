<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Model_System_Config_Source_Shipping_Method
 *
 */
class Clougistic_Connector_Model_System_Config_Source_Shipping_Method
{
    public function toOptionArray($dummy = null, $sort = SORT_ASC)
    {
        $carriers = Mage::getSingleton('shipping/config')->getActiveCarriers();
		$options = array();
		
        foreach ($carriers as $carrierCode => $carrier) {
			$methodOptions = array();
            foreach ($carrier->getAllowedMethods() as $methodCode => $method) {
				$key = sprintf('%s_%s', $carrierCode, $methodCode);
				$methodOptions[] = array(
					'value' => $key,
					'label' => $method
				);
			}
			
			$carrierTitle = Mage::getStoreConfig('carriers/' . $carrierCode . '/title');
			if (! $carrierTitle) $carrierTitle = $carrierCode;
			
			$options[] = array(
				'value' => $methodOptions,
				'label' => $carrierTitle
			);

        }

        if ($sort !== false && count($options) > 0) {
            foreach ($options as $key => $row) {
                $label[$key] = $row['label'];
            }

            array_multisort ($label, $sort, $options);
        }

        return $options;
    }
}