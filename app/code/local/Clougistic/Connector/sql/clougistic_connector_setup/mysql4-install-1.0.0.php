<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$connection = $installer->getConnection();
$connection->addColumn($installer->getTable('sales/order'), 'cg_status', 'tinyint not null default 0');
$connection->addColumn($installer->getTable('sales/order_grid'), 'cg_status', 'tinyint not null default 0');

$installer->endSetup();