<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$connection = $installer->getConnection();

try {
	$connection->addColumn($installer->getTable('sales/order_item'), 'cg_cost', 'decimal(12, 4) not null default 0');
} catch (Exception $e) {}

$connection->resetDdlCache();
$installer->endSetup();
