<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Helper_Api
 *
 */
class Clougistic_Connector_Helper_Api extends Mage_Core_Helper_Data
{
    const MODULE_CODE = 'Clougistic_Connector';

    # clougistic order statuses
	const ORDER_STATUS_NEW = 0;
	const ORDER_STATUS_PROCESSING = 1;
	const ORDER_STATUS_SHIPPED = 2;
	const ORDER_STATUS_ON_HOLD = 8;
	const ORDER_STATUS_CANCELED = 9;

    const ORDER_TYPE_SALE = 0;
    const ORDER_TYPE_SALE_POS = 1;
	
	# clougistic sync statuses
	const CLOUGISTIC_UNSYNCED = 0;
	const CLOUGISTIC_INVALID = 1;
	const CLOUGISTIC_ERROR = 2;
	const CLOUGISTIC_SYNCED = 3;
	const CLOUGISTIC_SYNC_PENDING = 6;

    const FIELD_ITEMS = 'items';
	const FIELD_USE_REFERENCE = 'use_reference';
	
	# order head fields
    const ORDER_FIELD_REFERENCE = 'reference';
    const ORDER_FIELD_CUSTOMER_ID = 'customer_id';
    const ORDER_FIELD_SOURCE_ID = 'source_id';
    const ORDER_FIELD_TYPE = 'type';
    const ORDER_FIELD_CUSTOM = 'custom_field';
    const ORDER_FIELD_COD_AMOUNT = 'cod_amount';
    const ORDER_FIELD_CURRENCY = 'currency';
    const ORDER_FIELD_STATUS = 'status';
    const ORDER_FIELD_FLAG_PARTIAL = 'flag_partial';
    const ORDER_FIELD_SHIPPING_METHOD = 'shipping_method';
    const ORDER_FIELD_OPTIONS = 'options';
    const ORDER_FIELD_OPTIONS_POS_ZONE_ID = '@pos_zone_id';
    const ORDER_FIELD_OPTIONS_CONSIGNMENT_ZONE_ID = '@consignment_zone_id';
    const ORDER_FIELD_CREATED_AT = 'created_at';
	const ORDER_FIELD_DELIVERY_AT = 'delivery_at';
	const ORDER_FIELD_VALIDATION_HASH = 'validation_hash';

	# clougistic pickup shipping method
	const ORDER_SHIPPING_METHOD_PICKUP = 'pickup';
	
	# billing address fields
    const ORDER_FIELD_BILLING_COMPANY = 'billing_company';
    const ORDER_FIELD_BILLING_FIRSTNAME = 'billing_firstname';
    const ORDER_FIELD_BILLING_LASTNAME = 'billing_lastname';
    const ORDER_FIELD_BILLING_STREET_FULL = 'billing_street_full';
    const ORDER_FIELD_BILLING_STREET = 'billing_street';
    const ORDER_FIELD_BILLING_HOUSENUMBER = 'billing_housenumber';
    const ORDER_FIELD_BILLING_HOUSENUMBER_EXT = 'billing_housenumber_ext';
    const ORDER_FIELD_BILLING_POSTCODE = 'billing_postcode';
    const ORDER_FIELD_BILLING_CITY = 'billing_city';
    const ORDER_FIELD_BILLING_REGION = 'billing_region';
    const ORDER_FIELD_BILLING_COUNTRY = 'billing_country';
    const ORDER_FIELD_BILLING_PHONE = 'billing_phone';
    const ORDER_FIELD_BILLING_EMAIL = 'billing_email';
	
	# shipping address fields
    const ORDER_FIELD_SHIPPING_COMPANY = 'shipping_company';
    const ORDER_FIELD_SHIPPING_FIRSTNAME = 'shipping_firstname';
    const ORDER_FIELD_SHIPPING_LASTNAME = 'shipping_lastname';
    const ORDER_FIELD_SHIPPING_STREET_FULL = 'shipping_street_full';
    const ORDER_FIELD_SHIPPING_STREET = 'shipping_street';
    const ORDER_FIELD_SHIPPING_HOUSENUMBER = 'shipping_housenumber';
    const ORDER_FIELD_SHIPPING_HOUSENUMBER_EXT = 'shipping_housenumber_ext';
    const ORDER_FIELD_SHIPPING_POSTCODE = 'shipping_postcode';
    const ORDER_FIELD_SHIPPING_CITY = 'shipping_city';
    const ORDER_FIELD_SHIPPING_REGION = 'shipping_region';
    const ORDER_FIELD_SHIPPING_COUNTRY = 'shipping_country';
    const ORDER_FIELD_SHIPPING_PHONE = 'shipping_phone';
    const ORDER_FIELD_SHIPPING_EMAIL = 'shipping_email';

	# order item fields
    const ORDER_ITEM_FIELD_SKU = 'sku';
    const ORDER_ITEM_FIELD_REFERENCE = 'reference';
    const ORDER_ITEM_FIELD_QTY = 'qty';
    const ORDER_ITEM_FIELD_WEIGHT = 'weight';
    const ORDER_ITEM_FIELD_DESCRIPTION = 'description';
    const ORDER_ITEM_FIELD_UNIT_PRICE = 'unit_price';
    const ORDER_ITEM_FIELD_CUSTOM_OPTIONS = 'custom_options';
    const ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA = 'option_data';
    const ORDER_ITEM_FIELD_CUSTOM_OPTION_SHIP_SEPARATELY = 'ship_separately';

	const ORDER_RMA_FIELD_REFERENCE = 'rma_reference';
	const ORDER_RMA_FIELD_USE_ITEM_REFERENCE = 'use_item_reference';
	const ORDER_RMA_FIELD_CANCEL_ITEMS = 'cancel_items';
		
	/**
     * @return bool
     */
	public function isAdmin()
	{
		return Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml';
	}

    /**
     * @return mixed
     */
	public function getAdminHostname()
    {
        $adminUrl = Mage::getUrl('adminhtml');
        $urlParts = parse_url($adminUrl);
        return @$urlParts['host'];
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function isEnabled()
    {
        return (bool)Mage::getStoreConfig('clougistic_wms/connector/enabled');
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return Mage::getConfig()->getModuleConfig(self::MODULE_CODE)->version;
    }

	/**
     * @param null $store
     * @return mixed
     */
    public function isDebug()
    {
        return (bool)Mage::getStoreConfig('clougistic_wms/connector/debug_mode');
    }

    /**
     * @param null $store
     * @return mixed
     */
	public function getCODMethods()
	{
		return explode(',', Mage::getStoreConfig('clougistic_wms/connector/cod_paymentmethods'));
	}
	
	/**
     * @param null $store
     * @return mixed
     */
	public function getPickupMethods()
	{
		return explode(',', Mage::getStoreConfig('clougistic_wms/connector/pickup_shippingmethods'));
	}
	
	/**
     * @param null $store
     * @return mixed
     */
	public function getExcludeShippingMethods()
	{
		return explode(',', Mage::getStoreConfig('clougistic_wms/connector/exclude_shippingmethods'));
	}

	/**
     * @param null $store
     * @return mixed
     */
	public function getExcludeCustomOptions($store = null)
	{
		return explode("\n", Mage::getStoreConfig('clougistic_wms/connector/exclude_custom_options', $store));
	}
	
    /**
     * @return mixed
     */
    public function getUrlEndPoint()
    {
        return Mage::getStoreConfig('clougistic_wms/connector/url_endpoint');
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getApiKey($store = null)
    {
        return Mage::getStoreConfig('clougistic_wms/connector/api_key');
    }
	
	/**
     * @param null $store
     * @return mixed
     */
    public function getSourceId($store = null)
    {
        return Mage::getStoreConfig('clougistic_wms/connector/source_id', $store);
    }

	/**
     * @param null $store
     * @return mixed
     */
    public function getEntityId($store = null)
    {
        return Mage::getStoreConfig('clougistic_wms/connector/entity_id', $store);
    }

	/**
     * @return mixed
     */
    public function getOrderDeliveryDateField()
    {
        return Mage::getStoreConfig('clougistic_wms/connector/order_delivery_date_field');
    }

	/**
     * @return mixed
     */	
	public function getProductCategoryField()
	{
		return Mage::getStoreConfig('clougistic_wms/connector/product_category_field');
	}

	/**
     * @param null $store
     * @return mixed
     */	
	public function getShipmentIncrementIdPrefix($store = null)
	{
		return Mage::getStoreConfig('clougistic_wms/connector/shipment_increment_id_prefix', $store);
	}

	/**
     * @param null $store
     * @return bool
     */
	public function isPOSEnabled($store = null)
	{
		return (bool)Mage::getStoreConfig('clougistic_wms/pos/enabled', $store);
	}
	
	/**
     * @param null $store
     * @return bool
     */
	public function getPOSZoneId($store = null)
	{
		return Mage::getStoreConfig('clougistic_wms/pos/zone_id', $store);
	}

	/**
     * @param null $store
     * @return mixed
     */
	public function getPOSShippingMethods($store = null)
	{
		return explode(',', Mage::getStoreConfig('clougistic_wms/pos/shippingmethods', $store));
	}

    /**
     * @return mixed
     */
    public function isTasksEnabled()
    {
        return Mage::getStoreConfig('clougistic_wms/tasks/enabled');
    }

    /**
     * @return mixed
     */
    public function getTasksBatchSize()
    {
        return Mage::getStoreConfig('clougistic_wms/tasks/batch_size');
    }

	/**
     * @param $order
     * @return bool
     */
	public function isPOSOrder(Mage_Sales_Model_Order $order)
	{
		$posShippingMethods = $this->getPOSShippingMethods($order->getStore());
		$shippingMethod = explode('_', $order->getShippingMethod())[0];
		return in_array($shippingMethod, $posShippingMethods);
	}
	
    /** 
	 * @param array $data
	 * @return bool
	 */
	public function isValidOrderData(array $data)
	{
		return count($data[self::FIELD_ITEMS]) > 0;
	}

    /**
     * @param array $data
     * @return bool
     */
    public function isValidOrderRmaData(array $data)
    {
        if (count($data[self::FIELD_ITEMS]) == 0) {
            return false;
        }

        $hasBackToInventory = false;
        foreach ($data[self::FIELD_ITEMS] as $orderItemId => $item) {
            if ($item['qty'] > 0 && $item['back_to_inventory']) {
                $hasBackToInventory = true;
                break;
            }
        }

        return $hasBackToInventory;
    }

    /**
     * @param $status
     * @return array
     */
    public function getCgStatuses()
    {
        return array(
            self::CLOUGISTIC_UNSYNCED => $this->getCgStatusText(self::CLOUGISTIC_UNSYNCED),
            self::CLOUGISTIC_INVALID => $this->getCgStatusText(self::CLOUGISTIC_INVALID),
            self::CLOUGISTIC_ERROR => $this->getCgStatusText(self::CLOUGISTIC_ERROR),
            self::CLOUGISTIC_SYNCED => $this->getCgStatusText(self::CLOUGISTIC_SYNCED),
            self::CLOUGISTIC_SYNC_PENDING => $this->getCgStatusText(self::CLOUGISTIC_SYNC_PENDING)
        );
    }

    /**
     * @param $status
     * @return mixed
     */
	public function getCgStatusText($status)
    {
        switch ($status) {
            case self::CLOUGISTIC_UNSYNCED:     return $this->__('Unsynced');
            case self::CLOUGISTIC_INVALID:      return $this->__('Invalid');
            case self::CLOUGISTIC_ERROR:        return $this->__('Sync Error');
            case self::CLOUGISTIC_SYNCED:       return $this->__('Synced');
            case self::CLOUGISTIC_SYNC_PENDING: return $this->__('Pending Sync');
        }
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
	public function getOrderValidationHash(Mage_Sales_Model_Order $order)
    {
	    $billing = $order->getBillingAddress();
        $shipping = $order->getShippingAddress();
        
        return sha1($order->getStatus() . $billing->getCompany() . $billing->getFirstname() . $billing->getMiddlename() . $billing->getLastname() . $billing->getStreetFull() .
                $billing->getPostcode() . $billing->getCity() . $billing->getRegion() . $billing->getCountryId() . $billing->getTelephone() . $billing->getEmail() .
                $shipping->getCompany() . $shipping->getFirstname() . $shipping->getMiddlename() . $shipping->getLastname() . $shipping->getStreetFull() .
                $shipping->getPostcode() . $shipping->getCity() . $shipping->getRegion() . $shipping->getCountryId() . $shipping->getTelephone() . $shipping->getEmail());
    }

	/**
	 * Get carrier selector
	 */
	public function getCarrierSelector() 
	{
		return Mage::getSingleton('clougistic_connector/carrier_selector');
	}
	
    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function prepareSyncOrder(Mage_Sales_Model_Order $order)
    {
        $result = $this->getOrderHead($order) +
            $this->getBillingAddress($order->getBillingAddress()) +
            $this->getShippingAddress($order->getShippingAddress());
			
        return $result;
    }

	/**
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @return array
     */
	public function prepareSyncCreditmemo(Mage_Sales_Model_Order_Creditmemo $creditmemo)
	{
		$order = $creditmemo->getOrder();
        $result = [
				self::ORDER_RMA_FIELD_REFERENCE => $creditmemo->getIncrementId(),
				self::ORDER_FIELD_REFERENCE => $order->getIncrementId(),
				self::FIELD_USE_REFERENCE => true,
				self::ORDER_RMA_FIELD_CANCEL_ITEMS => true,
				self::FIELD_ITEMS => $this->getOrderRmaItems($creditmemo)
			];
	
        return $result;
	}
	
    /**
     * @param $order
     * @return array
     */
    private function getOrderHead (Mage_Sales_Model_Order $order)
    {
        $result = array();
        if ($order) {
            $result = array(
                self::ORDER_FIELD_REFERENCE 		=> $order->getIncrementId(),
                self::FIELD_USE_REFERENCE           => true,
                self::ORDER_FIELD_CUSTOMER_ID 		=> (int)$order->getCustomerId(),
                self::ORDER_FIELD_COD_AMOUNT 		=> (float)$this->getCODAmount($order),
                self::ORDER_FIELD_CURRENCY 			=> $order->getStore()->getBaseCurrencyCode(),
                self::ORDER_FIELD_SHIPPING_METHOD 	=> $this->getShippingMethod($order),
                self::ORDER_FIELD_FLAG_PARTIAL 		=> (int)$order->getCanShipPartially(),
                self::ORDER_FIELD_CREATED_AT		=> $order->getCreatedAt(),
                self::ORDER_FIELD_DELIVERY_AT 		=> $this->getOrderDeliveryDate($order),
                self::ORDER_FIELD_VALIDATION_HASH	=> $this->getOrderValidationHash($order),
                self::FIELD_ITEMS 					=> $this->getOrderItems($order)
            );

			$consignmentData = $this->getOrderConsignmentData($order);
			$isConsignmentSale = false;
			if ($consignmentData) {
				$isConsignmentSale = $consignmentData->type == 'sale';
			}

			$posEnabled = $this->isPOSEnabled($order->getStore());
			if ($posEnabled && $this->isPOSOrder($order)) {
				$result[self::ORDER_FIELD_TYPE] = self::ORDER_TYPE_SALE_POS;
				$result[self::ORDER_FIELD_OPTIONS] = [
					self::ORDER_FIELD_OPTIONS_POS_ZONE_ID => $this->getPOSZoneId($order->getStore())
				];
			}
			elseif ($isConsignmentSale) {
				$result[self::ORDER_FIELD_TYPE] = self::ORDER_TYPE_SALE_POS;
				$result[self::ORDER_FIELD_OPTIONS] = [
					self::ORDER_FIELD_OPTIONS_POS_ZONE_ID => $consignmentData->zone
				];
			}			
			else {
				$result[self::ORDER_FIELD_TYPE] = self::ORDER_TYPE_SALE;

				$options = null;
				
				if ($carrierOptions = $this->getCarrierSelector()->getOrderOptions($order)) {
					$options = (array)$options + $carrierOptions;
				}
				
				if ($consignmentData) {
					$options = (array)$options + array(
						self::ORDER_FIELD_OPTIONS_CONSIGNMENT_ZONE_ID => $consignmentData->zone
					);
				} 
				
				$result[self::ORDER_FIELD_OPTIONS] = $options;
					
				if (($status = $this->getStatus($order)) !== null) {
					$result[self::ORDER_FIELD_STATUS] = $status;
				}
			}
        }
		
        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     */
	private function getStatus(Mage_Sales_Model_Order $order)
	{
		# if items are cancelled, order status can still be complete
		# order in clougistic has to be cancelled if this is the case
		# order items can not be cancelled individually, complete order only
		# this has nothing to do with creditmemos as these use qty_refunded
		
		$sumCancelled = 0;
		foreach ($order->getAllItems() as $item) {
			$sumCancelled += $item->getQtyCanceled();
		}
		if ($sumCancelled > 0) {
			return self::ORDER_STATUS_CANCELED;
		}
		
		# return conversion from magento state > cg status
		
		switch ($order->getState()) {
			case $order::STATE_NEW:
			case $order::STATE_PENDING_PAYMENT:
			case $order::STATE_PAYMENT_REVIEW:
				return self::ORDER_STATUS_NEW;
			case $order::STATE_PROCESSING:
				return self::ORDER_STATUS_PROCESSING;
			case $order::STATE_HOLDED:
				return self::ORDER_STATUS_ON_HOLD;
			case $order::STATE_CANCELED:
			case $order::STATE_CLOSED:
				return self::ORDER_STATUS_CANCELED;
			default:
				return null;
		}
	}
	
	/**
	 * Get product type singleton model
	 */
	private function getProductTypeModel()
	{
		return Mage::getSingleton('catalog/product_type');
	}

	/**
	 * Get product option singleton model
	 */
	private function getProductOptionModel()
	{
		return Mage::getSingleton('catalog/product_option');
	}
	
    /**
     * @param Mage_Sales_Model_Order $order
	 * @return array
     */
    private function getOrderItems (Mage_Sales_Model_Order $order)
    {
		$productTypeModel = $this->getProductTypeModel();
		
        $result = array();
        foreach ($order->getAllItems() as $item)
        {
            switch ($item->getProductType()) {
                case $productTypeModel::TYPE_BUNDLE:

					$container = array();
					$orderItem = $this->getOrderItem($item, 'bundle-head');
					if ($orderItem !== null) {
						$container[] = $orderItem;
					}
					
					$relatedItems = $item->getChildrenItems();
					$relatedItemsCount = 0;
					if (count($relatedItems) > 0) {
						foreach ($relatedItems as $relatedItem) {
							$orderItem = $this->getOrderItem($relatedItem, 'bundle-item');
							if ($orderItem !== null) {
								if (count($container) > 0) {
									$container[] = $orderItem;
								}
								else {
									$result[] = array($orderItem);
								}
								$relatedItemsCount++;
							}
						}

						if (count($container) > 0 && $relatedItemsCount > 0) {
							$result[] = $container;
						}
					}
					
                break;
				case $productTypeModel::TYPE_CONFIGURABLE:

					$relatedItems = $item->getChildrenItems();
					if (count($relatedItems) > 0) {
						$orderItem = $this->getOrderItem($relatedItems[0], 'configurable-item');
						if ($orderItem !== null) {
							$result[] = array($orderItem);
						}
                    }

				break;
                case $productTypeModel::TYPE_SIMPLE:
                case $productTypeModel::TYPE_GROUPED:
				
					if ($item->getParentItemId()) {
                        continue;
                    }
					
					$orderItem = $this->getOrderItem($item, 'simple-item');
					if ($orderItem !== null) {
						$result[] = array($orderItem);
					}
					
                break;
                default:
                    # do not sync virtual and downloadable items
                break;
            }
        }

        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $item
     * @return array
     */
    private function getOrderItem (Mage_Sales_Model_Order_Item $item, $type)
    {
        $result = array();
		
        if ($item) 
		{
            $result = array(
                self::ORDER_ITEM_FIELD_DESCRIPTION => $item->getName(),
                self::ORDER_ITEM_FIELD_WEIGHT => (float)$item->getWeight(),
                self::ORDER_ITEM_FIELD_QTY => (float)max($item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled() - $item->getQtyShipped(), 0)
            );
			
			switch ($type) {
				case 'bundle-head':
				
					if ($this->getShipSeparately($item)) {
						return null;
					}
					
					$result[self::ORDER_ITEM_FIELD_SKU] = $item->getSku();
					$result[self::ORDER_ITEM_FIELD_REFERENCE] = $item->getItemId();
					$result[self::ORDER_ITEM_FIELD_UNIT_PRICE] = (float)$item->getBasePrice();
					if ($customOptions = $this->getCustomOptions($item)) {
						$result[self::ORDER_ITEM_FIELD_CUSTOM_OPTIONS][self::ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA] = $customOptions;
					}
					
				break;
				case 'bundle-item':
				
					$parentItem = $item->getParentItem();
					
					if ($this->isExcludedItem($parentItem)) {
						return null;
					}
										
					$shipSeparately = $this->getShipSeparately($parentItem);
					
					$result[self::ORDER_ITEM_FIELD_SKU] = $item->getSku();
					if ($shipSeparately) {
						$result[self::ORDER_ITEM_FIELD_REFERENCE] = $item->getItemId();
					}
					$result[self::ORDER_ITEM_FIELD_UNIT_PRICE] = $shipSeparately ? (float)$item->getBasePrice() : 0;
					
					if ($customOptions = $this->getCustomOptions($parentItem)) {
						$result[self::ORDER_ITEM_FIELD_CUSTOM_OPTIONS][self::ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA] = $customOptions;
					}
					
				break;
				case 'configurable-item':
				
					$parentItem = $item->getParentItem();
					
					if ($this->isExcludedItem($parentItem)) {
						return null;
					}
					
					$result[self::ORDER_ITEM_FIELD_SKU] = $parentItem->getSku();
					$result[self::ORDER_ITEM_FIELD_REFERENCE] = $parentItem->getItemId();
					$result[self::ORDER_ITEM_FIELD_UNIT_PRICE] = (float)$parentItem->getBasePrice();
					
					if ($customOptions = $this->getCustomOptions($parentItem)) {
						$result[self::ORDER_ITEM_FIELD_CUSTOM_OPTIONS][self::ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA] = $customOptions;
					}
					
				break;
				case 'simple-item':
				
					if ($this->isExcludedItem($item)) {
						return null;
					}
				
					$result[self::ORDER_ITEM_FIELD_SKU] = $item->getSku();
					$result[self::ORDER_ITEM_FIELD_REFERENCE] = $item->getItemId();
					$result[self::ORDER_ITEM_FIELD_UNIT_PRICE] = (float)$item->getBasePrice();
					
					if ($customOptions = $this->getCustomOptions($item)) {
						$result[self::ORDER_ITEM_FIELD_CUSTOM_OPTIONS][self::ORDER_ITEM_FIELD_CUSTOM_OPTION_OPTION_DATA] = $customOptions;
					}
					
				break;				
				
			}
        }
		
        return $result;
    }
	
	private function IsExcludedItem($item)
	{
		if ($item) 
		{
			$productTypeModel = $this->getProductTypeModel();
			if ($item->getProductType() == $productTypeModel::TYPE_SIMPLE) {
				$product = Mage::getModel('catalog/product')->load($item->getProductId());
				if ($product) {
					# product attribute cg_exclude (boolean) (default false)
					return (bool)$product->getCgExclude();
				}
			}
		}
		
		return false;
	}
	
	public function getOrderRmaItems(Mage_Sales_Model_Order_Creditmemo $creditmemo)
	{
	    $result = array();
        foreach ($creditmemo->getAllItems() as $item)
        {
			$backToInventory = false;
			if ($item->hasBackToStock()) {
				$backToInventory = $item->getBackToStock();
			}
			
			if ($item->getQty() > 0) {
				$result[$item->getOrderItemId()] = array(
					'qty' => $item->getQty(),
					'back_to_inventory' => $backToInventory
				);
			}
		}
  
		return $result;
	}

	public function getShipSeparately(Mage_Sales_Model_Order_Item $item)
	{
		// SHIPMENT_SEPARATELY = 1;
		// SHIPMENT_TOGETHER = 0;
		
		$itemShipmentType = $item->getProductOptionByCode('shipment_type');
		
		if ($itemShipmentType != null) {
			$typeInstance = Mage::getModel('catalog/product_type')->factory($item->getProduct());	

			return $itemShipmentType == $typeInstance::SHIPMENT_SEPARATELY;
		}
		
		return null;
	}
	
	/**
	**/
	public function getOrderDeliveryDate(Mage_Sales_Model_Order $order)
	{
		$field = $this->getOrderDeliveryDateField($order->getStore());
		
		if ($field) {
			return $order->getData($field);
		}
		return null;
	}
	
	/**
	**/
	public function getOrderConsignmentData(Mage_Sales_Model_Order $order)
	{
		if ($order->getCustomerIsGuest()) {
			return null;
		}
		
		$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
		if ($consignmentZone = trim($customer->getConsignmentZone())) {
			$data = new StdClass;
			$data->zone = $consignmentZone;
			$data->type = strtolower($customer->getResource()->getAttribute('consignment_type')->getFrontend()->getValue($customer));
			return $data;
		}
		
		return null;
	}
	
    /**
     * @param Mage_Sales_Model_Order $order
     * @return int
     */
    private function getCustomOptions(Mage_Sales_Model_Order_Item $item)
    {
		/**
			simple/grouped
				verwijder de options met een sku als deze in de options staan
			config
				verwijder de options met een sku als deze in de parent options staan
		*/

		$productOptionModel = $this->getProductOptionModel();
		$productOptions = $item->getProduct()->getOptions();
		$itemOptions = $item->getProductOptionByCode('options');
		$excludedOptions = $this->getExcludeCustomOptions($item->getOrder()->getStore());
		$result = null;
		
		if (is_array($itemOptions) && is_array($productOptions)) 
		{
			foreach ($itemOptions as $index => $itemOption) 
			{
				if (in_array($itemOption['label'], $excludedOptions)) {
					continue;
				}
								
				$productOptionTypeValue = Mage::getModel('catalog/product_option_value')
					->getCollection()
					->addFieldToFilter('option_type_id', $itemOption['option_value'])
					->addFieldToFilter('option_id', $itemOption['option_id'])
					->getFirstItem();
				
				$productOptionValueHasSku = false;
				if ($productOptionTypeValue) {
					$productOptionValueHasSku = $productOptionTypeValue->getSku() ? true : false;
				}	
					
				$useThisOption = true;
				foreach ($productOptions as $productOption) 
				{
					if ($itemOption['option_id'] == $productOption->getId()) {
						if ($productOption->getSku() || $productOptionValueHasSku) {
							if (!in_array($productOption->getType(), array(
								$productOptionModel::OPTION_TYPE_FILE, 
								$productOptionModel::OPTION_TYPE_FIELD,
								$productOptionModel::OPTION_TYPE_AREA,
								$productOptionModel::OPTION_TYPE_DATE,
								$productOptionModel::OPTION_TYPE_TIME,
								$productOptionModel::OPTION_TYPE_DATE_TIME
							))) {
								$useThisOption = false;
							}
						}
						break;
					}
				}
				
				if ($useThisOption) {
					$result[] = array(
						'label' => $itemOption['label'],
						'value' => $itemOption['value']
					);
				}
			}
		}
		
		return $result;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return float|int
     */
    private function getCODAmount(Mage_Sales_Model_Order $order)
    {
        # check if COD payment and add COD amount field
        $paymentMethod = $order->getPayment()->getMethod();
        if (in_array($paymentMethod, $this->getCODMethods())) {
            return $order->getBaseGrandTotal();
        }
        return 0;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    private function getShippingMethod(Mage_Sales_Model_Order $order)
    {
        $shippingMethod = $order->getShippingMethod();
		
		if (in_array($shippingMethod, $this->getPickupMethods()) || stripos($shippingMethod, self::ORDER_SHIPPING_METHOD_PICKUP) !== false) {
			return self::ORDER_SHIPPING_METHOD_PICKUP;
		}
		
        return substr($shippingMethod, 0, strpos($shippingMethod, '_'));
    }
	
    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
	public function isExcludedShippingMethod(Mage_Sales_Model_Order $order)
	{
		return in_array($order->getShippingMethod(), $this->getExcludeShippingMethods());
	}	

    /**
     * @param Mage_Customer_Model_Address $address
     * @return array
     */
    private function getShippingAddress (Mage_Sales_Model_Order_Address $address)
    {
        $result = array();
        if ($address) {
            $result = array(
                self::ORDER_FIELD_SHIPPING_COMPANY => trim($address->getCompany()),
                self::ORDER_FIELD_SHIPPING_FIRSTNAME => trim($address->getFirstname()),
                self::ORDER_FIELD_SHIPPING_LASTNAME => trim(trim($address->getMiddlename()) . ' ' . trim($address->getLastname())),
                self::ORDER_FIELD_SHIPPING_STREET_FULL => trim($address->getStreetFull()),
                self::ORDER_FIELD_SHIPPING_POSTCODE => trim($address->getPostcode()),
                self::ORDER_FIELD_SHIPPING_CITY => trim($address->getCity()),
                self::ORDER_FIELD_SHIPPING_REGION => trim($address->getRegion()),
                self::ORDER_FIELD_SHIPPING_COUNTRY => trim($address->getCountryId()),
                self::ORDER_FIELD_SHIPPING_PHONE => str_replace(array(' ', '-', '(', ')' , '+'), array('', '', '', '', '00'), $address->getTelephone()),
                self::ORDER_FIELD_SHIPPING_EMAIL => trim($address->getEmail())
            );
        }
        return $result;
    }

    /**
     * @param Mage_Customer_Model_Address $address
     * @return array
     */
    private function getBillingAddress (Mage_Sales_Model_Order_Address $address)
    {
        $result = array();
        if ($address) {
            $result = array(
                self::ORDER_FIELD_BILLING_COMPANY => trim($address->getCompany()),
                self::ORDER_FIELD_BILLING_FIRSTNAME => trim($address->getFirstname()),
                self::ORDER_FIELD_BILLING_LASTNAME => trim(trim($address->getMiddlename()) . ' ' . trim($address->getLastname())),
                self::ORDER_FIELD_BILLING_STREET_FULL => trim($address->getStreetFull()),
                self::ORDER_FIELD_BILLING_POSTCODE => trim($address->getPostcode()),
                self::ORDER_FIELD_BILLING_CITY => trim($address->getCity()),
                self::ORDER_FIELD_BILLING_REGION => trim($address->getRegion()),
                self::ORDER_FIELD_BILLING_COUNTRY => trim($address->getCountryId()),
                self::ORDER_FIELD_BILLING_PHONE => str_replace(array(' ', '-', '(', ')' , '+'), array('', '', '', '', '00'), $address->getTelephone()),
                self::ORDER_FIELD_BILLING_EMAIL => trim($address->getEmail()),
            );
        }
        return $result;
    }
}