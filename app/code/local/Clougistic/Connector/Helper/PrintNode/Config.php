<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Helper_PrintNode_Config
 *
 */
class Clougistic_Connector_Helper_PrintNode_Config extends Mage_Core_Helper_Abstract
{
	/**
     * @param null $store
     * @return bool
     */
	public function isEnabled($store = null)
	{
		return (bool)Mage::getStoreConfig('clougistic_wms/printnode/enabled', $store);
	}
	
	/**
     * @param null $store
     * @return mixed
     */
	public function getEnabledDocuments($store = null)
	{
		return explode(',', Mage::getStoreConfig('clougistic_wms/printnode/documents', $store));
	}

	/**
     * @param $key
     * @param $document
     * @param null $store
     * @return bool
     */
	public function canPrintDocument($type, $document, $store = null)
	{
		$enabledDocuments = $this->getEnabledDocuments($store);
		$key = sprintf('%s_%s', $type, str_replace(' ', '_', strtolower($document)));
		return in_array($key, $enabledDocuments);
	}
}