<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Block_Adminhtml_Clougistic_Grid_Renderer_Status
 *
 */
class Clougistic_Connector_Block_Adminhtml_Clougistic_Grid_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
		$helper = Mage::helper('clougistic_connector/api');
		
        $status = $row->getData($this->getColumn()->getIndex());
		switch ($status) {
			case $helper::CLOUGISTIC_UNSYNCED:       $options = array('color' => '255,127,0'); break;
			case $helper::CLOUGISTIC_INVALID:        $options = array('color' => '255,127,0'); break;
			case $helper::CLOUGISTIC_ERROR:          $options = array('color' => '255,0,0'); break;
			case $helper::CLOUGISTIC_SYNCED:         $options = array('color' => '0,127,0'); break;
			case $helper::CLOUGISTIC_SYNC_PENDING:   $options = array('color' => '255,127,0'); break;
		}
        return sprintf('<span style="color:rgb(%s)">%s</span>', $options['color'], $helper->getCgStatusText($status));
    }
}