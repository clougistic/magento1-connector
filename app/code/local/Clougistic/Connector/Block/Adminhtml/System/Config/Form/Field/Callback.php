<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 *
 * Clougistic_Connector_Block_Adminhtml_System_Config_Form_Field_Callback
 *
 */
class Clougistic_Connector_Block_Adminhtml_System_Config_Form_Field_Callback extends Mage_Adminhtml_Block_System_Config_Form_Field implements Varien_Data_Form_Element_Renderer_Interface
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);

        return
<<<CBUTTON
    <button id="clougistic_wms_connector_test_callback" title="Test callback" type="button" onclick="testCgCallback()" style=""><span><span><span>Test Clougistic Callback</span></span></span></button>
    <script type="text/javascript">
    //<![CDATA[
    function testCgCallback() {
        new Ajax.Request('{$this->getUrl('*/clougistic_index/testCallback')}', {
            method:     'get',
            onSuccess: function(transport) {
                alert(transport.responseText);
            },
            onFailure: function(transport) {
                alert(transport.responseText);
            }
        });
    }
    //]]>
    </script>
CBUTTON;
    }

    /**
     * Render the element without a scope label
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     *
     * @see parent::render()
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->setScopeLabel('');
        return parent::render($element);
    }
}
