<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Block_Adminhtml_System_Config_Form_Field_Status
 *
 */
class Clougistic_Connector_Block_Adminhtml_System_Config_Form_Field_Status extends Mage_Adminhtml_Block_System_Config_Form_Field implements Varien_Data_Form_Element_Renderer_Interface
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);

        $systemInfo = null;
        $isWelcome = Mage::getSingleton('clougistic_connector/api')->greetings($systemInfo);

        $dummyInput = '<input id="clougistic_wms_connector_status" value="" type="hidden">';

        if ($isWelcome) {

            $systemInfoText = '';
            foreach ($systemInfo as $key => $value) {
                $systemInfoText .= sprintf("%s: %s\n", $key, $value);
            }

            return $dummyInput . '<label style="padding: 2px; background-color: black; color: #00ff00; width: 276px; display: block">Connected</label>'
                 . sprintf('<textarea style="font-family: consolas; width: 275px; height: 100px; height:overflow-x: hidden; overflow-y: scroll"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" readonly="readonly">%s</textarea>', $systemInfoText);
        }
        else {
            return $dummyInput . '<label style="padding: 2px; background-color: black; color: #ff0000; width: 276px; display: block">Not connected</label>';
        }
    }

    /**
     * Render the element without a scope label
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     *
     * @see parent::render()
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->setScopeLabel('');
        return parent::render($element);
    }
}
