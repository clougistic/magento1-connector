<?php

class Clougistic_Connector_Block_Adminhtml_System_Config_Form_Field_Version extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
    const HELP_URL = 'http://help.clougistic.com/magento-extension/configuration';

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_getFieldHtml($element);
    }

    protected function _getFieldRenderer()
    {
        if (empty($this->_fieldRenderer)) {
            $this->_fieldRenderer = Mage::getBlockSingleton('adminhtml/system_config_form_field');
        }
        return $this->_fieldRenderer;
    }

    protected function _getFieldHtml($fieldset)
    {
        $currentVer = Mage::helper('clougistic_connector/api')->getVersion();
        if (!$currentVer) {
            return '';
        }

        $field = $fieldset->addField('version', 'label', array(
            'label' => sprintf("%s (<a href='%s' target='_new'>Help</a>)", $this->__('Version'), self::HELP_URL),
            'value' => $currentVer,
        ))->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }
}