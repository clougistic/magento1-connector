<?php
/**
 * Clougistic Connector
 *
 * Author: Daniel van Els
 * Copyright: Clougistic
 * 
 * Clougistic_Connector_Adminhtml_Clougistic_IndexController
 *
 */
class Clougistic_Connector_Adminhtml_Clougistic_IndexController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Manually sync order with Clougistic
     */
    public function syncOrderAction()
    {
        $id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($id);

        if ($order) {
			Mage::getSingleton('clougistic_connector/api')->updateOrder($order, true);
        }

        $this->_redirect('*/sales_order/view', array('order_id' => $id));
    }

    /**
     * Test callback from Clougistic
     * This is used to check if the export api in Clougistic has been setup correctly
     * to enable bidirectional requests
     */
    public function testCallbackAction()
    {
        echo Mage::getSingleton('clougistic_connector/api')->testCallback();
        exit;
    }

    /**
     * Retry failed tasks in Clougistic
     */
    public function retryTasksAction()
    {
        echo Mage::getSingleton('clougistic_connector/api')->retryTasks();
        exit;
    }
}