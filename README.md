![clougistic](https://bitbucket.org/clougistic/magento1-connector/avatar/128)

## Clougistic WMS Connector for Magento 1

Install the Magento Clougistic WMS Connector extension

- 	This extension can only be used with Magento 1.
-	Backup your Magento installation and database before installation.
-	Git clone https://bitbucket.org/clougistic/magento1-connector.git or [download](https://bitbucket.org/clougistic/magento1-connector/downloads/) this repo in the root of your Magento installation.
-	Clear the Magento cache.
-	Logout and back in to the admin panel.
-	Configuration is under System > Configuration > Clougistic Extensions > WMS

# Help

More information about installation and configuration can be found in the [online help](http://help.clougistic.com/magento-extension).