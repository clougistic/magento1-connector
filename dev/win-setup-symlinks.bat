@echo off
REM specify target magento root folder on command line, run this file from extension/dev folder
@set TARGET=%~1

@if "%TARGET%" == "" goto missing_target

@echo Setting up symlinks for: %TARGET%

@if not exist "%TARGET%\app\code\local\" mkdir "%TARGET%\app\code\local"
@mklink /D "%TARGET%\app\code\local\Clougistic" "%~DP0\..\app\code\local\Clougistic"
@mklink /D "%TARGET%\skin\adminhtml\default\default\images\clougistic" "%~DP0\..\skin\adminhtml\default\default\images\clougistic"
@mklink "%TARGET%\app\etc\modules\Clougistic_Connector.xml" "%~DP0\..\app\etc\modules\Clougistic_Connector.xml"

goto done

:missing_target:
@echo Target folder missing.

:done
@echo done